#!/usr/bin/env python3
import os
from prometheus_client import start_http_server, Counter
import aio_pika
import asyncio
import sys
import hvac
import json


async def main(loop):

    start_http_server(8080)
    credential_path = 'secret/hostgroups/rabbitmq/systems-production/users/logs'

    c = Counter('icinga_messages', 'Icinga Messages',
                ['severity', 'message_type'])

    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']
    elif 'VAULT_OKD' in os.environ:
        sys.stderr.write("Using K8s auth")
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        vault.auth_kubernetes("icinga-responder",
                              jwt,
                              mount_point=os.environ.get('VAULT_OKD'))
    else:
        sys.stderr.write("Could not determin vault auth, quitting\n")
        return 5

    creds = vault.read(credential_path)['data']

    server = 'rabbitmq-04.oit.duke.edu'

    connection = await aio_pika.connect_robust(
        host=server,
        port=5671,
        login=creds['username'],
        password=creds['password'],
        virtualhost='logs',
        ssl=True,
        loop=loop,
    )

    async with connection:
        queue_name = 'icinga'

        # Creating channel
        channel = await connection.channel()  # type: aio_pika.Channel

        # Declaring queue
        queue = await channel.declare_queue(queue_name, durable=True)

        async with queue.iterator() as queue_iter:
            # Cancel consuming after __aexit__
            async for message in queue_iter:
                async with message.process():
                    # print(message.body)
                    data = json.loads("%s" % message.body.decode())
                    prefix = data['message'].split(']')[1].split(
                        ':')[0].strip()
                    try:
                        severity, message_type = prefix.split('/')
                    except Exception as e:
                        sys.stderr.write("Could not parse %s" % prefix)
                        continue
                    c.labels(severity, message_type).inc()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
